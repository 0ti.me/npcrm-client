const Vue = require('vue').default;

const axios = require('axios');
const config = require('config');

const home = require('./src/routes/home');
const notFound = require('./src/routes/not-found');

const routes = {'/home': home};

new Vue({
  el: '#app',
  computed: {
    ViewComponent() {
      return routes[this.currentRoute] || notFound;
    },
  },
  data() {
    return {
      currentRoute: window.location.pathname,
      errored: false,
      info: {},
      loading: true,
    };
  },
  mounted() {
    axios
      .get(`${config.apiServer.baseUri}/someData`)
      .then(response => (this.info.someData = response.data.bpi))
      .catch(error => (this.errored = true && console.log(error)))
      .finally(() => (this.loading = false));
  },
  render(h) {
    return h(this.ViewComponent);
  },
});
