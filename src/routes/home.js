module.exports = {
  template: `
  <div id="app">
    {{ message }}
    {{ failed ? 'failed' : 'succeeded' }}
  </div>
`,
};
